# HTTP hidden service

Implements a simple HTTP/1.1 and SOCKS5 proxy onion service.

Credits:
[ynuwenhof](https://github.com/ynuwenhof/) for the socks5 code.
